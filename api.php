<?php
$method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : null;
switch ($method) {
    case 'POST':
        //Headers
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json');
        header('Access-Control-Allow-Methods: POST');
        header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-With');
        include_once('class/Database.Class.php');
        include_once('class/Create.Class.php');
        $db     = new Database;
        $con    = $db->con();
        //get the raw posted data.
        $data   = json_decode(file_get_contents("php://input"));
        $create = new Create($con);
        $create->setDetails($data->name, $data->gender, $data->age);
        if ($create->create()) {
            die(json_encode(array(
                "Message" => "Details Saved"
            )));
        } else {
            die(json_encode(array(
                "Message" => "Details Not Saved"
            )));
        }
        break;
    case 'PUT':
        //Headers
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json');
        header('Access-Control-Allow-Methods: PUT');
        header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-With');
        include_once('class/Database.Class.php');
        include_once('class/Create.Class.php');
        $db     = new Database;
        $con    = $db->con();
        //get the raw posted data.
        $data   = json_decode(file_get_contents("php://input"));
        $create = new Create($con);
        $create->setDetails($data->name, $data->gender, $data->age);
        if ($create->update($data->id)) {
            die(json_encode(array(
                "Message" => "Details Updated"
            )));
        } else {
            die(json_encode(array(
                "Message" => "Details Not Updated"
            )));
        }
        break;
    case 'DELETE':
        //Headers
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json');
        header('Access-Control-Allow-Methods: DELETE');
        header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-With');
        include_once('class/Database.Class.php');
        include_once('class/Create.Class.php');
        $db     = new Database;
        $con    = $db->con();
        //get the raw posted data.
        $data   = json_decode(file_get_contents("php://input"));
        $create = new Create($con);
        if ($create->delete($data->id)) {
            die(json_encode(array(
                "Message" => "Details Deleted"
            )));
        } else {
            die(json_encode(array(
                "Message" => "Details Not Deleted"
            )));
        }
        break;
    default:
        break;
}
?>