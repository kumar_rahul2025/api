<?php 
		
		require_once('constant.php');
	class Rest 
	{

		public $request;
		public $param;
		public $serviceName;

		public function __construct()
		{
			if($_SERVER['REQUEST_METHOD'] !== 'POST')
			{
				$this->throwError(REQUEST_METHOD_NOT_VALID, 'Not Corrected Method');
			}
			
			$handler = fopen('php://input','r');
			
			$this->request = stream_get_contents($handler);
			
			$this->validateRequest();
		}

		public function validateRequest()
		{
			if($_SERVER['CONTENT_TYPE'] !== 'application/json')
			{
				$this->throwError(REQUEST_CONTENTTYPE_NOT_VALID, 'Incorrect dataType');
			}
			
			$data = json_decode($this->request,true);
			
			if(!isset($data['name']) || $data['name'] =='')
			{
				$this->throwError(API_NAME_REQUIRED,'API_NAME_REQUIRED');	
			}
			
			$this->serviceName = $data['name'];

			if(!is_array($data['param']))
			{
				$this->throwError(API_PARAM_REQUIRED,'API_PARAM_REQUIRED');	
			}

			$this->param = $data['param'];

		}
		public function processApi()
		{
			$api = new Api;
			$rMethod = new ReflectionMethod('Api',$this->serviceName);
			if(!method_exists($api, $this->serviceName))
			{
				$this->throwError(106,'API DOES NOT EXIST');
			}
			$rMethod->invoke($api);

		}

		public function validateParameters($fieldName,$value,$dataType,$required = true)
		{
			if( $required==true && empty($value) == true)
			{
				$this->throwError(VALIDATE_PARAM_REQUIRED,"$fieldName is required");
			}

			switch ($dataType) {

				case 'BOOLEAN':

						if(!is_bool($dataType))
						{
							$this->throwError(VALIDATE_PARAM_DATATYPE,'INVALID DATATYPE required');	
						}

					break;

				case 'INTEGER':
					if(!is_numeric($dataType))
						{
							$this->throwError(VALIDATE_PARAM_DATATYPE,'INVALID DATATYPE ');	
						}
					break;
				case 'STRING':

					if(!is_string($dataType))
						{
							$this->throwError(VALIDATE_PARAM_DATATYPE,'INVALID DATATYPE required' );	
						}
						break;	
				default:
					break;
			}
			return $value;

		}

		public function throwError($code,$message)
		{
			header('content-type:application/json');

			$errorMessage = json_encode(['status'=>$code,'message'=>$message]);
			
			die($errorMessage);

		}

		public function returnResponse($data,$message=null)
		{
        	header('Content-Type: application/json');
        	$response = json_encode(['response'=>['status'=>$data,$message]]);
			echo $response;
			exit;
		}


	}

?>