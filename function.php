<?php

spl_autoload_register(function($classname){

	 $path = 'class/'.$classname.".Class.php";


	if(file_exists($path))
	{
		require_once($path);
	}
	else
	{
		die($path.'Not found');
	}

}) ;
?>